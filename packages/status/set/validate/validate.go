package validate

import (
	"digitalocean/status-set/models"
	"errors"
	"os"
	"strconv"
	"strings"
)

func Validate(message *models.Message) (string, error) {
	chatID, err := strconv.ParseInt(os.Getenv("TELEGRAM_CHAT_ID"), 10, 64)
	if err != nil {
		return "", err
	}

	if message.Chat.ID != chatID {
		return "", errors.New("wrong chat")
	}

	text := message.Text
	if len(text) < 7 {
		return "", errors.New("undefined command type")
	}
	if text[0:7] != "/status" {
		return "", errors.New("undefined command")
	}

	status := strings.ToLower(text[7:])
	return status, nil
}
