package main

import (
	"context"
	"digitalocean/status-set/models"
	"digitalocean/status-set/redis"
	"digitalocean/status-set/telegram"
	val "digitalocean/status-set/validate"
)

func Main(ctx context.Context, update models.Update) models.Response {
	message := update.Message

	status, err := val.Validate(message)
	if err != nil {
		return telegram.SendMessage("error: "+err.Error(), update)
	}

	if err := redis.SetStatus(ctx, status); err != nil {
		return telegram.SendMessage("error: "+err.Error(), update)
	}

	return telegram.SendMessage("Successfully set status - "+status, update)
}
