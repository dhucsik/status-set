package telegram

import "digitalocean/status-set/models"

type sendMessageRequest struct {
	ChatID           int64  `json:"chat_id"`
	Text             string `json:"text"`
	ReplyToMessageID int    `json:"reply_to_message_id"`
}

func createRequest(text string, update models.Update) sendMessageRequest {
	return sendMessageRequest{
		ChatID:           update.Message.Chat.ID,
		Text:             text,
		ReplyToMessageID: update.Message.ID,
	}
}
