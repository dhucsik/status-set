package telegram

import (
	"digitalocean/status-set/models"
	"net/http"
	"net/url"
	"os"
	"strconv"
)

const tgbotAPIURL = "https://api.telegram.org/bot"

func SendMessage(text string, update models.Update) models.Response {
	return send(createRequest(text, update))
}

func send(req sendMessageRequest) models.Response {
	sendURL := tgbotAPIURL + os.Getenv("TELEGRAM_BOT_TOKEN") + "/sendMessage"

	_, err := http.PostForm(sendURL, url.Values{
		"chat_id":             {strconv.FormatInt(req.ChatID, 10)},
		"text":                {req.Text},
		"reply_to_message_id": {strconv.Itoa(req.ReplyToMessageID)},
	})
	if err != nil {
		return models.Response{Message: "Error: " + err.Error()}
	}

	return models.Response{Message: "success"}
}
