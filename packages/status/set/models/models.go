package models

type Update struct {
	Message *Message `json:"message,omitempty"`
}

type Message struct {
	ID   int    `json:"message_id,omitempty"`
	Chat *Chat  `json:"chat,omitempty"`
	Text string `json:"text,omitempty"`
}

type Chat struct {
	ID int64 `json:"id,omitempty"`
}

type Response struct {
	Message string `json:"message,omitempty"`
}
